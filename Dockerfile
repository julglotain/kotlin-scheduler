FROM maven:alpine

COPY pom.xml .
RUN mvn -B dependency:resolve-plugins dependency:resolve

COPY . .

RUN mvn package

CMD ["java","-jar","target/kotlin-scheduler-0.0.1-SNAPSHOT.jar"]