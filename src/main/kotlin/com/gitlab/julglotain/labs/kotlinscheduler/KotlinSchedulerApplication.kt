package com.gitlab.julglotain.labs.kotlinscheduler

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@SpringBootApplication
@EnableScheduling
class KotlinSchedulerApplication

@Component
class Scheduler {

    @Scheduled(fixedDelayString = "\${delay:1000}")
    fun ping() {
        println("ping")
    }

}

fun main(args: Array<String>) {
    runApplication<KotlinSchedulerApplication>(*args)
}
